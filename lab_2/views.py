from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = 'Hi, I am Tatag Aziz Prawiro, a student at FASILKOM UI. Please\
 accept me as a developer in ITDEV of CompFest X, thank you. Hehe.'

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'index_lab2.html', response)
