from django.conf.urls import url
from .views import index, add_friend, validate_npm, delete_friend, friend_list, list_teman, friend_detail

urlpatterns = [
     url(r'^$', index, name='index'),
     url(r'^add-friend/$', add_friend, name='add-friend'),
     url(r'^validate-npm/$', validate_npm, name='validate-npm'),
     url(r'delete-friend/(?P<friend_id>[0-9]+)/$', delete_friend, name='delete-friend'),
     url(r'^get-friend-list/$', friend_list, name='get-friend-list'),
     url(r'^list-teman/$', list_teman, name='list-teman'),
     url(r'friend-detail/$', friend_detail, name='friend-detail'),
]
