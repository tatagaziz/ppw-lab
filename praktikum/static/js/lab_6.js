const themes = [
			{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
			{"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
			{"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
			{"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
			{"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
			{"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
			{"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
			{"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
			{"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
			{"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
			{"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
		];
const defaultTheme = {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#212121"};


$(document).ready(function(){
	if(!localStorage.themes) {
		localStorage.themes = JSON.stringify(themes);
		localStorage.selectedTheme = JSON.stringify(defaultTheme);
	}
	var applied = JSON.parse(localStorage.selectedTheme);
	$("body").css({"background-color" : applied.bcgColor, "color" : applied.fontColor});	
	$(".my-select").select2({
		'data' : JSON.parse(localStorage.themes)
	});
	$('.apply-button').on('click', function(){  // sesuaikan class button
    // [TODO] ambil value dari elemen select .my-select
    var data = JSON.parse(localStorage.themes);
    var id = $(".my-select").select2("val");
    // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
    $.each(data, function(i, theme){
    	if(id == theme.id){
    		// [TODO] ambil object theme yang dipilih

    		// [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
    		$("body").css({"background-color" : theme.bcgColor, "color" : theme.fontColor})

    		// [TODO] simpan object theme tadi ke local storage selectedTheme
    		var selected = {
					"id":theme.id,
					"bcgColor":theme.bcgColor,
					"fontColor" : theme.fontColor
			};
			localStorage.selectedTheme = JSON.stringify(selected);
			return false;
    	}
    });
    
});

	$("textarea#idtextarea").keypress(function(e) {
		var isi = $("textarea#idtextarea").val();
	    if (e.keyCode == '13') {
	        $("div.msg-insert").append("<p class ='msg-send'>"+isi+"</p>");
	        $("textarea#idtextarea").val("");
	        return false;
    	}
	});
	$("#cdaarrow").on("click", function(){
		$(".chat-body").toggle();
	});
});

// Calculator
var erase = false;

var go = function(x) {
	var print = document.getElementById('print');
	if (x === 'ac') {
		/* implemetnasi clear all */
	    print.value = "";
	    erase = true;
  	} else if (x === 'eval') {
    	print.value = Math.round(evil(print.value) * 10000) / 10000;
    	erase = true;
  	} else if (x === 'sin'){
  		print.value = print.value % 360;
  		print.value = Math.sin(print.value * Math.PI/180);
  		if(print.value === '1.2246467991473532e-16'){
  			print.value = 0;
  		}
  	} else if (x === 'tan'){
  		print.value = print.value % 360;
  		print.value = Math.tan(print.value * Math.PI/180);
  		if(print.value === '16331239353195370'){
  			print.value = 'Infinite';
  		}
  		if(print.value === '-1.2246467991473532e-16'){
  			print.value = 0;
  		}
  		if(print.value === '5443746451065123'){
  			print.value = '-Infinite';
  		}

  	} else if (x === 'log'){
  		print.value = Math.log10(print.value);
  	}
  	else {
		print.value += x;
	}
};

	function evil(fn) {
		return new Function('return ' + fn)();
	}
// END
console.log("HEHE");